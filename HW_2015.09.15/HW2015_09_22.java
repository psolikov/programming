
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by user on 12/28/2015.
 */
public class HW2015_09_22 {
    public static void EdgeList(int[][] matrix, int n) { //Печать оринтированного графа с матрицей смежности длины n
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[j][i] != 0) System.out.println((i + 1) + " ---> " + (j + 1));
            }
        }
    }

    public static void ShortestWay1(int[][] massive, int n) { //Определение кратчайшего пути межжду 1 и n вершиной в ориентированном графе
        int[][] d = massive.clone();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                d[i][j] = massive[i][j];
                if (d[i][j] == 0) {
                    d[i][j] = 1000 * 1000 * 1000;
                }
            }
        }
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    d[i][j] = Math.min(d[i][j], d[i][k] + d[k][j]);
                }
            }
        }
        System.out.println(d[n - 1][0]);
    }

    public static int sum1 = 0;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.in"));
        int n = sc.nextInt();
        int[][] m = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[j][i] = sc.nextInt();
            }
        }
        int[][] mas = m;

        EdgeList(m, n);
        System.out.println();
        System.out.println("Кратайший путь межу (1) и (" + n + ") вершиной");
        ShortestWay1(m, n);
    }
}
