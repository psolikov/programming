import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by user on 12/28/2015.
 */
public class Proba {

    public static int sum1 = 0;

    public static boolean D2W(int[][] matrix, int n) {
        boolean[] used = new boolean[n];
        for (int i = 0; i < n; i++) used[i] = false;
        DFS(0, matrix, used, n);
        if (sum1 > 1) return true;
        else return false;
    }

    public static void DFS(int v, int[][] mam, boolean[] used, int n) {
        used[v] = true;
        for (int i = 0; i < n; i++) {
            if ((i == n - 1) && (mam[v][i] != 0)) sum1++;
            if (!used[i] && (mam[v][i] != 0)) {
                DFS(i, mam, used, n);
            }
        }
    }

    public static int frk(int v, int[][] mem, int n) {
        Queue q = new LinkedList<Integer>();
        boolean[] used = new boolean[n];
        used[v] = true;
        q.add(v);
        int now = 0;
        while (!q.isEmpty()) {
            now = (int) q.poll();
            int sum = 0;
            for (int i = 0; i < n; i++) {
                if ((!used[i]) && (mem[now][i] != 0)) {
                    sum++;
                    q.add(i);
                    used[i] = true;
                }
            }
            if (sum > 1) return now;
        }
        if (now != n-1) return now;
        else
            return -1;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.in"));
        int n = sc.nextInt();
        int[][] m = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[j][i] = sc.nextInt();
            }
        }
        int[][] mas = m;

        System.out.println("В графе несколько путей из (1) в (" + n + ") вершину --- " + D2W(mas, n));
        System.out.println();
        System.out.println("Ближайшая развилка --- " + frk(0, mas, n));

    }
}
