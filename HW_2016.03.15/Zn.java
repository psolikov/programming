import java.util.Scanner;

/**
 * Created by user on 3/27/2016.
 */
public class Zn {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите n (Zn), затем число a.");
        int n = sc.nextInt();
        int a = sc.nextInt();
        for (int i = 1; i<n; i++){
            if (mult(i,i,n)==clas(a,n)) System.out.println(i);
        }
    }

    public static int mult(int x, int y, int n){
        return x*y%n;
    }

    public static int clas(int x, int n ){
        return x%n;
    }
}
