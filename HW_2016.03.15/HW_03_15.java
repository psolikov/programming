import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 3/27/2016.
 */
public class HW_03_15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BigInteger n = sc.nextBigInteger();
        System.out.println(frac(n));
        System.out.println(phi(n));
        System.out.println("Ax+By=1. Введите коэффициенты A и B. Они должны быть взаимнопростыми, иначе нет корней.");
        BigInteger a = sc.nextBigInteger();
        BigInteger b = sc.nextBigInteger();
        if(a.gcd(b).equals(BigInteger.ONE)) {
            BigInteger x = pow(a,phi(b).add(BigInteger.ONE.negate()));
            BigInteger y = BigInteger.ONE.add(pow(a,phi(b)).negate()).divide(b);
            System.out.print("x = "+ x+" y = "+ y);
        }
        else System.out.print("Нет решений");
    }

    public static ArrayList<BigInteger> frac(BigInteger n) {
        BigInteger k = BigInteger.valueOf(2);
        ArrayList<BigInteger> mas = new ArrayList<BigInteger>();
        mas.add(BigInteger.ONE);
        if (n == BigInteger.ONE) return mas;
        while (n.mod(BigInteger.ONE.add(BigInteger.ONE)) == BigInteger.ZERO) {
            n = n.divide(BigInteger.ONE.add(BigInteger.ONE));
//            System.out.println(2);
            mas.add(k);

        }
        k = BigInteger.ONE.add(BigInteger.ONE).add(BigInteger.ONE);
        while (n.compareTo(BigInteger.ONE) >= 1) {
            while (n.mod(k) == BigInteger.ZERO) {
                mas.add(k);
//                System.out.println(k + " ");
                n = n.divide(k);
            }
            k = k.add(BigInteger.valueOf(2));
        }
        return mas;
    }

    public static BigInteger phi(BigInteger n) {
        if (n.equals(BigInteger.ONE)) return BigInteger.ONE;
        else {
            ArrayList<BigInteger> primes = frac(n);
            if (primes.size() <= 2) return n.add(BigInteger.ONE.negate());
            else if (kol(primes,1) == primes.size()-1) return n.add((primes.get(1).pow(primes.size()-2)).negate());
            else {
                BigInteger prod = BigInteger.ONE;
                for (int i = 1; i < primes.size(); i++) {
                    int l = kol(primes,i);
                    prod = prod.multiply(phi(primes.get(i).pow(l)));
                    i+=l-1;
                }
                return prod;
            }
        }
    }

    public static int kol(ArrayList<BigInteger> mas, int index){
       int kolvo = 1;
        for (int i = index+1; i< mas.size(); i++){
           if (mas.get(i).equals(mas.get(index))) kolvo++;
           else break;
       }
        return kolvo;
    }

    public static BigInteger pow(BigInteger base, BigInteger exponent) {
        BigInteger result = BigInteger.ONE;
        while (exponent.signum() > 0) {
            if (exponent.testBit(0)) result = result.multiply(base);
            base = base.multiply(base);
            exponent = exponent.shiftRight(1);
        }
        return result;
    }

}
