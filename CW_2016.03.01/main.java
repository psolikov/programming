import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by user on 3/1/2016.
 */
public class main {
    public static void main(String[] args) {
        BigInteger a = BigInteger.valueOf(2);;
        BigInteger b = BigInteger.valueOf(2);
        a = a.pow(564); 
        a = a.subtract(BigInteger.ONE);
        b = b.pow(2016);
        b = b.subtract(BigInteger.ONE);
        System.out.println(GCD(a,b,1,0));
    }


    public static int eq(BigInteger a, BigInteger b, int anum, int bnum){
        if (a.compareTo(b) == 0) return anum;
        else{
            if (a.compareTo(b)>0){
                return eq(a.subtract(b),b,anum,bnum+anum);
            }
            else {
                return eq(a,b.subtract(a),anum+bnum,bnum);
            }
        }
    }

    public static BigInteger gcdBig(BigInteger a, BigInteger b){
        if (a != BigInteger.ZERO && b != BigInteger.ZERO)
        {
            if (a.compareTo(b) > 0) return gcdBig(a.remainder(b),b);
            else return gcdBig(a,b.remainder(a));
        }
        else
        {
            return a.add(b);
        }
    }

    public static int GCD(BigInteger a, BigInteger b, int anum, int bnum){
        while (a.compareTo(b) != 0){
            if (a.compareTo(b)>0){
                a = a.subtract(b);
                anum = anum + bnum;
            }
            else{
                b = b.subtract(a);
                bnum = bnum+anum;
            }
        }
        return anum;
    }


    public static int gcd(int a, int b){
        if (a!=0 && b!=0){
            if (a>b) return gcd(a%b,b);
            else return gcd(a,b%a);
        }
        else return a+b;
    }

    public static int gcd_m(int a, int b){
        if (a == b) return a;
        else{
            if (a>b){
                a = a-b;
//                bnum++;
                return gcd_m(a,b);
            }
            else {
                b = b-a;
//                anum++;
                return gcd_m(a,b);
            }
        }
    }

    public static int gcd_m2(int a, int b, int anum, int bnum){
        while (a!=b){
            if (a>b){
                a = a-b;
                anum = anum + bnum;
            }
            else{
                b = b-a;
                bnum = bnum+anum;
            }
        }
        return anum;
    }
}
