import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class NetSet{

    public static void main(String[] args){
        InetAddress IP;
        try {
            IP = InetAddress.getLocalHost();
            System.out.println("Your ip - " + IP);

            NetworkInterface NI = NetworkInterface.getByInetAddress(IP);
            byte[] mac = NI.getHardwareAddress();
            System.out.print("Your mac - ");
            StringBuilder s = new StringBuilder();
            for (int i = 0; i<mac.length; i++){
                s.append(String.format("%02X%s", mac[i], (i<mac.length-1)? "-":""));
            }
            System.out.print(s.toString());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

}