import java.util.Scanner;

public class HW_01_19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(isIP(s)+ " --- is IP");
        System.out.println(isMask(s)+ " --- is mask");
        System.out.print(isMAC(s)+ " --- is mac");
    }

    public static boolean isIP(String s) {
        if (dots(s) == 3) {
            int j = 0;
            boolean o = true;
            for (int i = 0; i < 3; i++) {
                String k = "";
                while (s.charAt(j) != '.') {
                    k = k.concat(String.valueOf(s.charAt(j)));
                    j++;
                }
                try {
                    int l = Integer.parseInt(k);
                    if (l <= 255 && l >= 0) o = o & true;
                    else o = false;
                } catch (NumberFormatException e) {
                    o = false;
                }
                j++;
            }
            String k = "";
            for (int i = j; i < s.length(); i++) {
                k = k.concat(String.valueOf(s.charAt(i)));
            }
            try {
                int l = Integer.parseInt(k);
                if (l <= 255 && l >= 0) o = o & true;
                else o = false;
            } catch (NumberFormatException e) {
                o = false;
            }
            return o;
        } else return (false);
    }

    public static boolean isMAC(String s){
        if (dash(s) == 5) {
            int j = 0;
            boolean o = true;
            for (int i = 0; i < 5; i++) {
                String k = "";
                while (s.charAt(j) != '-') {
                    k = k.concat(String.valueOf(s.charAt(j)));
                    j++;
                }
                try {
                    int l = Integer.parseInt(k,16);
                    if (l <= 255 && l >= 0) o = o & true;
                    else o = false;
                } catch (NumberFormatException e) {
                    o = false;
                }
                j++;
            }
            String k = "";
            for (int i = j; i < s.length(); i++) {
                k = k.concat(String.valueOf(s.charAt(i)));
            }
            try {
                int l = Integer.parseInt(k,16);
                if (l <= 255 && l >= 0) o = o & true;
                else o = false;
            } catch (NumberFormatException e) {
                o = false;
            }
            return o;
        } else return (false);
    }

    public static boolean isMask(String s) {
        if (isIP(s)) {
            int j = 0;
            String k = "";
            for (int i = 0; i < 3; i++) {
                while (s.charAt(j) != '.') {
                    k = k.concat(String.valueOf(s.charAt(j)));
                    j++;
                }
            }
            for (int i = j; i < s.length(); i++) {
                k = k.concat(String.valueOf(s.charAt(i)));
            }
            j = 0;
            while (k.charAt(j)=='1'){
                j++;
            }
            String m = k.substring(j);
            if (m.contains("1")){
                return false;
            }
            else return true;
        } else return false;
    }

    public static int dots(String s) {
        int kol = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '.') kol++;
        }
        return kol;
    }

    public static int dash(String s) {
        int kol = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '-') kol++;
        }
        return kol;
    }
}