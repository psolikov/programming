import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by user on 12/9/2015.
 */
public class Music {

    public static void main(String[] args) throws IOException {
        File wav = new File("input.wav");
        FileInputStream input = new FileInputStream(wav);
        FileOutputStream output = new FileOutputStream(new File("output.wav"));
        byte[] mas = new byte[(int) wav.length()];
        byte[] blockAlign = new byte[2];
        byte[] subchunk2Size = new byte[4];
        input.read(mas);
        blockAlign[0] = mas[32];
        blockAlign[1] = mas[33];
        subchunk2Size[0] = mas[40];
        subchunk2Size[1] = mas[41];
        subchunk2Size[2] = mas[42];
        subchunk2Size[3] = mas[43];

        short sample = 0;
        ByteBuffer bb = ByteBuffer.wrap(blockAlign);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        while (bb.hasRemaining()) {
            short v = bb.getShort();
            sample = v;
        }
        int size = 0;
        ByteBuffer bu = ByteBuffer.wrap(subchunk2Size);
        bu.order(ByteOrder.LITTLE_ENDIAN);
        while (bu.hasRemaining()) {
            int v = bu.getInt();
            size = v;
        }

        for (int i = 0; i < 44; i++) {
            output.write(mas[i]);
        }

        for (int i = size + 43; i >= 44 + sample - 1; i -= sample) {
            for (int j = i - sample + 1; j <= i; j++) {
                output.write(mas[j]);
            }
        }

    }
}
